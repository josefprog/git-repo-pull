'use strict'

let request = require('request-promise-native')

const projectApi = 'https://api.bitbucket.org/2.0/teams/progfin-ondemand/projects/?pagelen=100'
const repoApi = 'https://api.bitbucket.org/2.0/repositories/progfin-ondemand?q=project.key=%22{{projectKey}}%22&pagelen=100'

// Key 22Dxcpx68nazqsq4nF
// Secret scCwtt9da4DzQPbAZW6sF2PHZ7ZJNgQ4

// CURL
// curl -X POST -u "client_id:scCwtt9da4DzQPbAZW6sF2PHZ7ZJNgQ4" https://bitbucket.org/site/oauth2/access_token -d grant_type=authorization_code -d code={code}

console.log('Calling BitBucket Project List')
request(projectApi)
  .then(function (projectList) {
    // Parse out project files
    console.log('Projects received')

    parseRepos(projectList)
    .then(function (repoList) {
      // All Content Retrieved from Bitbucket
      // Display Results here
      console.log('\r\n---------------------------------------')

      repoList.forEach(function (repo) {
        console.log('Project: ' + repo.project + ' Repo: ' + repo.name)
        console.log('Description: ' + repo.description)
        console.log('Owner: ' + repo.owner)
        console.log('Clone URL: ' + repo.clone_url)
        console.log('---------------------------------------')
      })
    })
  })
  .catch(function (err) {
    console.log('Error occured: ' + err)
  })

function parseRepos (projectList) {
  // Return a Promise here
  return new Promise(function (resolve, reject) {
    let repoList = []
    let repoPromises = []

    // loop for each project slug
    JSON.parse(projectList).values.forEach(function (project) {
      // console.log('Project Name: ' + project.key)

      // request repo api for project slug
      let repoUrl = repoApi.replace('{{projectKey}}', project.key)

      repoPromises.push(request(repoUrl))
    }) // End forEach Loop

    Promise.all(repoPromises)
        .then(function (repoResults) {
          // forEach resultset
          repoResults.forEach(function (repoString) {
            // add repos to repo list
            let repoSet = JSON.parse(repoString)
            if (repoSet.values.length !== 0) {
              repoSet.values.forEach(function (repo) {
                if (repo.name != null) {
                  let repoItem = {
                    name: repo.name,
                    description: repo.description,
                    owner: repo.owner.display_name,
                    clone_url: repo.links.clone[0].href,
                    html_url: repo.links.html.href
                  }

                  repoList.push(repoItem)
                }
              })
            }
          })
        })
        .then(function () {
          resolve(repoList)
        })
  })
}
